﻿using System;

namespace Ocorrencias
{
    class Program
    {
        static void Main(string[] args)
        {
            int elementos;

            do
            {
                Console.Write("Insira o número de elementos que irá inserir:");

            } while (!int.TryParse(Console.ReadLine(), out elementos));

            //declaração dos arrays
            int[] tabela = new int[elementos];


            for (int i = 0; i < tabela.Length; i++)
            {
                int elemento;

                do
                {
                    Console.Write($"Insira o elemento {i + 1} :");

                } while (!int.TryParse(Console.ReadLine(), out elemento));

                tabela[i] = elemento;
            }

            int N = tabela.Length - 1;
            int[] conta = new int[N + 1];

            //inicializar o contador de elementos, pressupondo que cada
            //elemento ocorre apenas uma vez
            for (int i = 0; i < N + 1; i++)
                conta[i] = 1;

            for (int i = 0; i < N - 1; i++)
            {
                if (conta[i] != 0)
                {
                    //se um elemento for igual ao próximo, incrementar o contador
                    //do elemento e colocar a zero o contador do próximo elemento
                    for (int j = i + 1; j <= N; j++)
                    {
                        if (tabela[i] == tabela[j])
                        {
                            conta[i]++;
                            conta[j] = 0;
                        }
                    }
                }
            }

            //mostrar os contadores que sejam diferentes de zero
            for (int i = 0; i < conta.Length; i++)
            {
                if (conta[i] != 0)
                {
                    Console.WriteLine($"{tabela[i]} ocorre {conta[i]} vez(es) na tabela.\n");
                }
            }

            Console.ReadKey();
        }
    }
}
